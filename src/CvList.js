import React from 'react';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import Paper from 'material-ui/Paper';
import List, {ListItem} from 'material-ui/List';

const cvs = [
    {
        firstName: 'Max'
        , lastName: 'Arshinov'
        , position: 'Senior Full-Stack Developer'
        , about: 'Max has been a team leader on numerous successful projects, including NYSE Trading system and ' +
            'products oriented towards financial market. He excels in both back-end and front-end development, ' +
            'as well as test automation. He is also an expert with JavaScript and C#, and has often created ' +
            'website back-end features from scratch.'
        , education: 'Bachelor of Engineering degree in Computer Science.'
        , university: 'Kazan State Technical University'
        , employment: [
            { title: 'CTO', company: 'Boommy Fashion', years: '2013 - 2014' }
            , { title: 'QA Practice Leader', company: 'Etna Software', years: '2012 - 2013' }
            , { title: 'Senior .NET Developer', company: 'Memos Outsourcing', years: '2011 - 2012' }
            , { title: '.NET Developer', company: 'Concert.ru', years: '2010 - 2011' }
            , { title: 'Web Developer', company: 'SmartHead', years: '2008 - 2010' }
            , { title: 'Web Developer', company: 'Laconix', years: '2006 - 2008' }
        ]
        , skills: [
            { title: 'C#', years: '9 years' }
            , { title: 'JavaScript', years: '8 years' }
            , { title: 'ASP.NET MVC', years: '8 years' }
            , { title: 'HTML, CSS, jQuery', years: '5 years' }
            , { title: 'ReactJS', years: '1 year' }
            , { title: 'AngularJS', years: '1 year' }
            , { title: 'WebDriver', years: '4 years' }
            , { title: 'WCF', years: '5 years' }
            , { title: 'SQL', years: '8 years' }
            , { title: 'Agile Software Development', years: '5 years' }
            , { title: 'Behavior-driven Development (BDD)', years: '4 years' }
        ]
    }
    , {
        firstName: 'Andrey'
        , lastName: 'Pesoshin'
        , about: 'As a software developer he worked with various high-load projects oriented towards international market, written in PHP, ' +
            'Ruby on Rails and .NET. During academic work he has gained additional experience in embedded systems ' +
            'development (GNU C and C++, FPGA, VHDL) and Java.'
        , position: 'Senior .NET Developer'
        , education: 'Andrey is an inventor and holds PhD degree as he has defended a thesis in 2015'
        , university: 'Kazan State Technical University'
        , employment: [
            { title: 'Senior .NET Developer', company: 'Boommy Fashion', years: '2013 - 2014' }
            , { title: 'CTO', company: 'Discover IT', years: '2012 - 2013' }
            , { title: 'Web Developer', company: 'FlatStack', years: '2009 - 2012' }
        ]
        , skills: [
            { title: 'C#', years: '4 years' }
            , { title: 'PHP', years: '4 years' }
            , { title: 'Ruby On Rails', years: '4 years' }
            , { title: 'VHDL/Verilog.', years: '2 years' }
        ]
    }
    , {
        firstName: 'Alexey'
        , lastName: 'Shvorak'
        , about: 'Senior software developer with solid experience in modern JavaScript, PHP and .NET platform.'
        , position: 'Senior Full-Stack Developer'
        , education: 'Bachelor of Engineering degree in Computer Science.'
        , university: 'Kazan (Volga) Federal University, Kazan'
        , employment: [
            { title: 'Senior Full-Stack Developer', company: '33slona.ru', years: '2014 - 2015' }
            , { title: 'Web Developer', company: 'ITAR-TASS Media Agency', years: '2013 - 2014' }
            , { title: 'Web Developer', company: 'SmartHead', years: '2012 - 2013' }
        ]
        , skills: [
            { title: 'AngularJS', years: '2 years' }
            , { title: 'ReactJS', years: '1 year' }
            , { title: 'TypeScript', years: '1 year' }
            , { title: 'JavaScript', years: '4 years' }
            , { title: 'HTML, CSS, jQuery', years: '3 years' }
            , { title: 'NodeJS', years: '3 years' }
            , { title: 'PHP', years: '3 years' }
            , { title: 'C#', years: '2 years' }
            , { title: 'SQL', years: '4 years' }
        ]
    }
    , {
        firstName: 'Dias '
        , lastName: 'Mukhametzyanov'
        , about: 'Software Developer with experience in low-level programming.'
        , position: '.NET Developer'
        , education: 'Master of Engineering degree in Computer Science.'
        , university: 'Kazan (Volga) Federal University, Kazan'
        , employment: [
            { title: 'NC/PLC programmer', company: 'SM Controls', years: '2015 - 2016' }
            , { title: 'Lead programmer', company: 'GradoService, LLC', years: '2013 - 2015' }
            , { title: 'PHP developer', company: 'F1 Soft', years: '2012 - 2013' }
            , { title: 'T-SQL developer', company: 'AstralBit', years: '2011 - 2011' }

        ]
        , skills: [
            { title: 'C#', years: '2 years' }
            , { title: 'C++', years: '3 years' }
            , { title: 'PHP', years: '1 years' }
            , { title: 'JavaScript', years: '1 years' }
            , { title: 'SQL', years: '4 years' }
        ]
    }
];




const skillList = cv => cv.skills.map((skill) => <li>{skill.title} <small>{skill.years}</small></li>);
const employmentList = cv => cv.employment.map((e) => <li>{e.title} {e.company} <small>{e.years}</small></li>);
const fullName = cv => cv.firstName + ' ' + cv.lastName;

const styleFirst = {
    paddingLeft: 40,
}
const style = {
    display: 'table-cell',
    padding: 20,
    margin: 20,
    float: 'left',
};

const overflowStyle = {
    overflow: 'hidden'
}

const list  = cvs.map((cv) =>
    <ListItem key={cv.name}>
        <Card style={overflowStyle}>
            <CardTitle style={styleFirst} title={fullName(cv)} subtitle={cv.position}/>
            <Paper zDepth={0} style={styleFirst}>
                <h4>About {cv.firstName}</h4>
                {cv.about}
            </Paper>
            <Paper zDepth={0} style={style}>
                <h4>Experience</h4>
                {skillList(cv)}
            </Paper>
            <Paper zDepth={0} style={style}>
                <h4>Education & Employment</h4>
                <p>{cv.university}</p>
                <p>{cv.education}</p>
                <hr/>
                {employmentList(cv)}
            </Paper>
        </Card>
    </ListItem>
);

const CvList = () => (
    <List>
        {list}
    </List>
);

export default CvList;