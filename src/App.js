import React from 'react';
import theme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import DocumentTitle from 'react-document-title';

import AppBar from 'material-ui/AppBar';

import CvList from './CvList';
const App = () => (
    <DocumentTitle title="CV List">
      <MuiThemeProvider muiTheme={getMuiTheme(theme)}>
        <div>
          <AppBar title="CV List" showMenuIconButton={false}/>
          <CvList/>
        </div>
      </MuiThemeProvider>
    </DocumentTitle>
);

export default App;
